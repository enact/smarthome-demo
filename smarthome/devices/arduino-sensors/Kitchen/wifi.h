#ifndef __WIFI_H__
#define __WIFI_H__

#include "w600.h"

class Wifi {
  public:
    Wifi();
    void configure();
    int connectToAP(const char *wifi_ssid, const char *wifi_passwd);
    int createSocket(const char*target_ip, uint16_t target_port, uint16_t local_port);
    bool send(int socket, char *char_val);
	
  protected:
    AtWifi _wifi;
};

#endif
