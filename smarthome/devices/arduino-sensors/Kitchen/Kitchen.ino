// Grove Light Sensor v1.2
// https://wiki.seeedstudio.com/Grove-Light_Sensor/

// Grove Gaz Sensor v1.5
//https://wiki.seeedstudio.com/Grove-Gas_Sensor/

// Grove Temperature Humidity Sensor (DHT11)
// https://wiki.seeedstudio.com/Grove-TemperatureAndHumidity_Sensor/
#include "DHT.h"

#define DHTPIN  4
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

// Grove W600 Wifi
// https://github.com/Seeed-Studio/Seeed_Arduino_W600
#include "wifi.h"
#include "wifi_ext.h"
#include "wifi_ids.h"

WifiExt wifi;

int socket = -1;

//----------------------------------------

void setup() {
  debug.begin(115200);
  // Init sensors
  dht.begin();

  // Init Wifi
  wifi.configure();
  wifi.connectToAP(Wifi_Ssid, Wifi_Passwd);
  socket = wifi.createSocket("\"192.168.0.27\"", 1881, 1234);
  
  debug.println("Initialization finished.");
}

unsigned long startTime, currentTime, elapsedTime;
#define LOOP_PERIOD 1500

void loop() {
  // Reset timer
  startTime = millis();

  Heartbeat();
  Light_Sensor();
  Gas_Sensor();
  DHT_Sensor();

  // Get elapsed time
  currentTime = millis();
  elapsedTime = currentTime - startTime;

  // If elaspse time < 1s then wait a little bit to have a new measure every second.
  if (elapsedTime < LOOP_PERIOD) {
    delay(LOOP_PERIOD - elapsedTime);
  }
}

void Heartbeat() {
  wifi.send(socket, "Heartbeat", 1);
}

#define MULTIPLE_SENSOR_READ(value, fct_get_value, wait_sec, nb_iter) do { \
  for (int i=0; i < (nb_iter); i++) { \
    (value) += (fct_get_value); \
    delay(wait_sec); \
  } \
  (value) = (value) / (nb_iter); \
} while (0)

#define WIFI_SEND(last_value, value, topic) do { \
  if ((value) != (last_value)) { \
    wifi.send(socket, topic, (value)); \
    (last_value) = (value); \
  } else { \
    debug.println(value); \
  } \
} while (0)

void Light_Sensor() {
  static int last_value = 0;
  int value = 0;

  MULTIPLE_SENSOR_READ(value, analogRead(A2), 2, 10);
  int lightValue = map(value, 0, 800, 0, 100);
  
  WIFI_SEND(last_value, lightValue, "Brightness");
}

void Gas_Sensor() {
  static float last_value = 0;
  int sensorValue = 0;
  float value;

  MULTIPLE_SENSOR_READ(sensorValue, analogRead(A6), 2, 10);
  value = (float)sensorValue / 1024;
  value = round(value * 100) / 100; // Rounded off to the hundredth

  WIFI_SEND(last_value, value, "Gas Density");
}

void DHT_Sensor() {
  static float last_humidity = 0;
  static float last_temperature = 0;
  
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  // Compute heat index in Celsius (isFahreheit = false)
  //float hic = dht.computeHeatIndex(t, h, false);

  WIFI_SEND(last_humidity, h, "Humidity");
  WIFI_SEND(last_temperature, t, "Temperature");
}
