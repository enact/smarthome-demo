// Grove Light Sensor v1.2
// https://wiki.seeedstudio.com/Grove-Light_Sensor/

// Grove HCHO Sensor v1.0
// https://wiki.seeedstudio.com/Grove-HCHO_Sensor/
#define Vc 4.95
//the number of R0 you detected as described in the manual
#define R0 35.54

// Grove Temperature Humidity Sensor Pro (DHT22)
// https://wiki.seeedstudio.com/Grove-Temperature_and_Humidity_Sensor_Pro/
#include "DHT.h"

#define DHTPIN  4
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

// Grove W600 Wifi
// https://github.com/Seeed-Studio/Seeed_Arduino_W600
#include "wifi.h"
#include "wifi_ext.h"
#include "wifi_ids.h"

WifiExt wifi;

int socket = -1;

//----------------------------------------

void setup() {
  debug.begin(115200);
  dht.begin();

  // Init Wifi
  wifi.configure();
  wifi.connectToAP(Wifi_Ssid, Wifi_Passwd);
  socket = wifi.createSocket("\"192.168.0.27\"", 1881, 1234);

  debug.println("Initialization finished.");
}

unsigned long startTime, currentTime, elapsedTime;
#define LOOP_PERIOD 1500

void loop() {
  // Reset timer
  startTime = millis();

  Heartbeat();
  Light_Sensor();
  HCHO_Sensor();
  DHT_Sensor();

  // Get elapsed time
  currentTime = millis();
  elapsedTime = currentTime - startTime;

  // If elaspse time < 1s then wait a little bit to have a new measure every second.
  if (elapsedTime < LOOP_PERIOD) {
    delay(LOOP_PERIOD - elapsedTime);
  }
}

void Heartbeat() {
  wifi.send(socket, "Heartbeat", 1);
}

#define MULTIPLE_SENSOR_READ(value, fct_get_value, wait_sec, nb_iter) do { \
  for (int i=0; i < (nb_iter); i++) { \
    (value) += (fct_get_value); \
    delay(wait_sec); \
  } \
  (value) = (value) / (nb_iter); \
} while (0)

#define WIFI_SEND(last_value, value, topic) do { \
  if ((value) != (last_value)) { \
    wifi.send(socket, topic, (value)); \
    (last_value) = (value); \
  } else { \
    debug.println(value); \
  } \
} while (0)

void Light_Sensor() {
  static int last_value = 0;
  int value = 0;

  MULTIPLE_SENSOR_READ(value, analogRead(A2), 2, 10);
  int lightValue = map(value, 0, 800, 0, 100);

  WIFI_SEND(last_value, lightValue, "Brightness");
}

void HCHO_Sensor() {
  static float last_rs = 0;
  static float last_ppm = 0;
  int sensorValue = 0;
  float value;

  sensorValue += analogRead(A6);
  sensorValue = sensorValue / 20;

  float Rs = (1023.0 / sensorValue) - 1;
  if (Rs != last_rs) {
    wifi.send(socket, "HCHO Rs", Rs);
    last_rs = Rs;
  } else {
    debug.println(Rs);
  }

  float ppm = pow(10.0, ((log10(Rs / R0) - 0.0827) / (-0.4807)));
  ppm = round(ppm * 100) / 100; // Rounded off to the hundredth
  if ( ppm != last_ppm) {
    wifi.send(socket, "HCHO ppm", ppm);
    last_ppm = ppm;
  } else {
    debug.println(ppm);
  }
}

void DHT_Sensor() {
  static float last_humidity = 0;
  static float last_temperature = 0;

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  // Compute heat index in Celsius (isFahreheit = false)
  //float hic = dht.computeHeatIndex(t, h, false);

  if (h != last_humidity) {
    wifi.send(socket, "Humidity", h);
    last_humidity = h;
  } else {
    debug.println(h);
  }

  if (t != last_temperature) {
    wifi.send(socket, "Temperature", t);
    last_temperature = t;
  } else {
    debug.println(t);
  }
}
