// Grove Light Sensor v1.2
// https://wiki.seeedstudio.com/Grove-Light_Sensor/

// Grove Sunlight Sensor v1.0
// https://wiki.seeedstudio.com/Grove-Sunlight_Sensor/
#include <Wire.h>
#include "SI114X.h"
SI114X SI1145 = SI114X();

// Grove W600 Wifi
// https://github.com/Seeed-Studio/Seeed_Arduino_W600
#include "wifi.h"
#include "wifi_ext.h"
#include "wifi_ids.h"

WifiExt wifi;

int socket = -1;

//----------------------------------------

void setup() {
  debug.begin(115200);

  // Init Sill45
  debug.println("Beginning Si1145 initialisation");
  while (!SI1145.Begin()) {
    debug.println("Si1145 is not ready!");
    delay(500);
  }
  debug.println("Si1145 is ready!");

  // Init Wifi
  wifi.configure();
  wifi.connectToAP(Wifi_Ssid, Wifi_Passwd);
  socket = wifi.createSocket("\"192.168.0.27\"", 1881, 1234);

  debug.println("Initialization finished.");
}

unsigned long startTime, currentTime, elapsedTime;
#define LOOP_PERIOD 1500

void loop() {
  // Reset timer
  startTime = millis();

  Heartbeat();
  Light_Sensor();
  SunLight_Sensor();

  // Get elapsed time
  currentTime = millis();
  elapsedTime = currentTime - startTime;

  // If elaspse time < 1s then wait a little bit to have a new measure every second.
  if (elapsedTime < LOOP_PERIOD) {
    delay(LOOP_PERIOD - elapsedTime);
  }
}

void Heartbeat() {
  wifi.send(socket, "Heartbeat", 1);
}

#define MULTIPLE_SENSOR_READ(value, fct_get_value, wait_sec, nb_iter) do { \
    for (int i=0; i < (nb_iter); i++) { \
      (value) += (fct_get_value); \
      delay(wait_sec); \
    } \
    (value) = (value) / (nb_iter); \
  } while (0)

#define WIFI_SEND(last_value, value, topic) do { \
    if ((value) != (last_value)) { \
      wifi.send(socket, topic, (value)); \
      (last_value) = (value); \
    } else { \
      debug.println(value); \
    } \
  } while (0)

void Light_Sensor() {
  static int last_value = 0;
  int value = 0;

  MULTIPLE_SENSOR_READ(value, analogRead(A2), 2, 10);
  int lightValue = map(value, 0, 800, 0, 100);

  WIFI_SEND(last_value, lightValue, "Brightness");
}

void SunLight_Sensor() {
  static int last_visible = 0, last_IR = 0;
  int visible, IR;
  static float last_UV;
  float UV;

  visible = SI1145.ReadVisible();
  IR = SI1145.ReadIR();
  UV = (float)SI1145.ReadUV() / 100;

  if ( (visible < (last_visible - 1)) || (visible > (last_visible + 1)) ) {
    WIFI_SEND(last_visible, visible, "Visible");
  }
  if ( (IR < (last_IR - 1)) || (IR > (last_IR + 1)) ) {
    WIFI_SEND(last_IR, IR, "IR");
  }
  if ( (UV < (last_UV - 0.01)) || (UV > (last_UV + 0.01)) ) {
    WIFI_SEND(last_UV, UV, "UV");
  }
}
