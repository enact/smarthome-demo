#!/bin/bash

sudo apt update
sudo apt upgrade
sudo apt install git
git clone https://github.com/respeaker/seeed-voicecard.git
cd seeed-voicecard
sudo ./install.sh --compat-kernel
echo "Press enter to reboot..."
read
reboot
