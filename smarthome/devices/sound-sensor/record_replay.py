import os
import pyaudio
import numpy
import paho.mqtt.client as mqtt
import scipy.io.wavfile as wavfile
from envparse import env

REPLAY = False
filename = None
TOPIC_SOUND = "enact/sensors/microphone/sound"
TOPIC_REPLAY = "enact/actuators/microphone/replay"
TOPIC_RECORD = "enact/actuators/microphone/record"

DOCKER_VARENV=['MQTT_HOST', 'MQTT_PORT']

def on_message_replay(client, userdata, message):
    global REPLAY
    global filename
    msg = message.payload.decode("utf-8")
    if msg == "false":
        REPLAY = False
        print("Replay Stropped", flush=True)
    else :
        REPLAY = True
        filename = msg
        print("Replaying", flush=True)

def on_connect(client, userdata, flags, rc):
	switcher = {
	    1: 'incorrect protocol version',
		2: 'invalid client identifier',
		3: 'server unavailable',
		4: 'bad username or password',
		5: 'not authorised'
	}
	if rc==0:
		print("MQTT broker connection OK.")
	else:
		print("MQTT broker bad connection: ", switcher.get(rc, "Unknown return code"))

def on_disconnect(client, userdata, rc):
	print("MQTT broker disconnecting: " + str(rc))
	print("MQTT broker: will automatically reconnect")

## MQTT broker connexion
if set(DOCKER_VARENV).issubset(set(os.environ)):
	MQTT_HOST = env(DOCKER_VARENV[0], default='localhost')
	MQTT_PORT = env.int(DOCKER_VARENV[1], default=1883)

client = mqtt.Client("record", clean_session=True)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.connect(MQTT_HOST, MQTT_PORT, keepalive=1800)
client.message_callback_add(TOPIC_REPLAY, on_message_replay)
print("Connected to MQTT", flush=True)
client.subscribe(TOPIC_REPLAY, qos=0)

# 
RATE=44100
RECORD_SECONDS = 1
CHUNKSIZE = 2048
CHANNELS = 2

# initialize portaudio
p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNKSIZE)
client.loop_start()
while True :
    if REPLAY == False:
        frames = [] # A python-list of chunks(numpy.ndarray)
        for _ in range(0, int(RATE / CHUNKSIZE * RECORD_SECONDS)):
            data = stream.read(CHUNKSIZE)
            frames.append(numpy.frombuffer(data, dtype=numpy.int16))
        #Convert the list of numpy-arrays into a 1D array (column-wise)
        numpydata = numpy.hstack(frames)
        client.publish(TOPIC_SOUND, numpydata.tobytes())
    else :
        fs_rate, s = wavfile.read(filename)
        # slice signal by fs_rate samples (1s duration)
        samples = len(s)//fs_rate
        client.publish(TOPIC_RECORD, "record")
        for i in range(samples): 
            if REPLAY == False:
                break
            start = i*fs_rate
            stop  = start + fs_rate - 1 
            numpydata = numpy.hstack(s[start:stop])
            client.publish(TOPIC_SOUND, numpydata.tobytes())
            # time.sleep(1)
        client.publish(TOPIC_RECORD, "stop record")
        REPLAY = False
        print("Replaying is Over", flush=True)
