#ifndef _ROLLERSHUTTERSSELECT_H_
#define _ROLLERSHUTTERSSELECT_H_
#include "RollerShutterSelect_sm.h"

#ifdef __cplusplus
extern "C" {
#endif

struct RollerShutterSelect {
  struct RollerShutterSelectContext _fsm;
};

void RollerShutterSelect_Init(struct RollerShutterSelect *rl);

#ifdef __cplusplus
}
#endif
#endif
