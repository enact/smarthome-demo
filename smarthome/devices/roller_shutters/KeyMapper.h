#ifndef _KEYMAPPER_H_
#define _KEYMAPPER_H_

#define KEY_1 2     // Relay for Key "1" on remote control connected to D2 (Key 1 = Kitchen roller shutter)
#define KEY_2 3     // Relay for Key "2" on remote control connected to D3 (Key 2 = Living Room bigest roller shutter)
#define KEY_3 4     // Relay for Key "3" on remote control connected to D4 (Key 3 = Living Room smaller roller shutter)
#define KEY_6 5     // Relay for Key "6" on remote control connected to D5 (Key 6 = all)
#define KEY_Up 6    // Relay for Key "Up" on remote control connected to D6
#define KEY_Stop 7  // Relay for Key "Stop" on remote control connected to D7
#define KEY_Down 8  // Relay for Key "Down" on remote control connected to D8

#ifdef __cplusplus
extern "C" {  // this line will be compiled only if included in a .cpp file and not in a .c
#endif

  void keyPress(short pin);

#ifdef __cplusplus
}
#endif

#endif
