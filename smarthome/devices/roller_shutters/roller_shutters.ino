// Added to enable printf function to have effect
// we need fundamental FILE definitions and printf declarations
#include <stdio.h>

// create a FILE structure to reference our UART output function
static FILE uartout = {0};

// create a output function
// This works because Serial.write, although of type virtual, already exists.
static int uart_putchar (char c, FILE *stream) {
  Serial.write(c);
  return 0;
}

#include "KeyMapper.h"
#include "RollerShutter.h"
#include "RollerShutterSelect.h"

struct RollerShutter rollerShutter[7];
struct RollerShutterSelect rollerShutterSelect;

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  // fill in the UART file descriptor with pointer to writer.
  fdev_setup_stream (&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);

  // The uart is the standard output device STDOUT.
  stdout = &uartout ;

  pinMode(KEY_1, OUTPUT);
  pinMode(KEY_2, OUTPUT);
  pinMode(KEY_3, OUTPUT);
  pinMode(KEY_6, OUTPUT);
  pinMode(KEY_Up, OUTPUT);
  pinMode(KEY_Stop, OUTPUT);
  pinMode(KEY_Down, OUTPUT);

  for (int i = 0; i < 7; i++) {
    RollerShutter_Init(&rollerShutter[i]);
  }
  RollerShutterSelect_Init(&rollerShutterSelect);
}

void eventDispatcher(char key) {
  static short last = 0;
  switch (key) {
    case '1':
      RollerShutterSelectContext_Select_1(&(&rollerShutterSelect)->_fsm);
      if (last != 1) {
        RollerShutter_Init(&rollerShutter[1]);
      }
      last = 1;
      break;
    case '2':
      RollerShutterSelectContext_Select_2(&(&rollerShutterSelect)->_fsm);
      if (last != 2)
        RollerShutter_Init(&rollerShutter[2]);
      last = 2;
      break;
    case '3':
      RollerShutterSelectContext_Select_3(&(&rollerShutterSelect)->_fsm);
      if (last != 3)      
        RollerShutter_Init(&rollerShutter[3]);
      last = 3;
      break;
    case '6':
      RollerShutterSelectContext_Select_All(&(&rollerShutterSelect)->_fsm);
      if (last != 6)
        RollerShutter_Init(&rollerShutter[6]);
      last = 6;
      break;
    case 'U':
      RollerShutterContext_Up(&(&rollerShutter[last])->_fsm); 
      break;
    case 'S':
      RollerShutterContext_Stop(&(&rollerShutter[last])->_fsm); 
      break;
    case 'D':
      RollerShutterContext_Down(&(&rollerShutter[last])->_fsm); 
      break;
    default: printf("Error\n");
  }
}

#define BUFF_SIZE 16

void loop() {
  char str[BUFF_SIZE]; // for incoming serial data
  size_t n;
  
  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    n = Serial.readBytesUntil('\n', str, BUFF_SIZE);
    for (int i = 0 ; i < n ; i++) {
      eventDispatcher(str[i]);
    }
  }
}
