#---------------
# Install system
#---------------

# Flash Raspbian with balenaEtcher
# Create "ssh" file in the boot partition
# Create "wpa_supplicant.conf" in the boot partition with the following text
##
country=fr
update_config=1
ctrl_interface=/var/run/wpa_supplicant

network={
scan_ssid=1
ssid=""
psk=""
}
##

# Boot Raspberry Pi 3B+ with the created SD Card
# ssh pi@raspberrypi.local

#---------------
# Sytem configure
#---------------
sudo apt update && sudo apt upgrade && sudo apt clean
sudo raspi-config
# configure timezone

#---------------
# Docker
#---------------

# Install current docker programs to get dependencies
curl -fsSL https://get.docker.com | sh

sudo usermod -aG docker pi

sudo mkdir -p /etc/systemd/system/docker.service.d
sudo sh -c 'echo "[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock
" > /etc/systemd/system/docker.service.d/startup_options.conf'

#sudo sh -c 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'

sudo systemctl daemon-reload
sudo systemctl enable docker.service

sudo apt install docker-compose

#---------------
# Monitoring
#---------------

smartspace

### On Synology

# connect to influxdb
influx -username admin -password admin

CREATE DATABASE raspi_smartspace_telegraf
USE raspi_smartspace_telegraf
CREATE USER raspi_smartspace_telegraf WITH PASSWORD 'raspi_smartspace_telegraf'
GRANT ALL ON raspi_smartspace_telegraf TO raspi_smartspace_telegraf
quit

exit

### On Raspberry Pi
cd /opt
sudo mkdir telegraf
sudo chown pi:pi telegraf
cd telegraf
scp pi@openhab.local:/opt/telegraf/* .
nano docker-compose.yml
# change hostname: openhab -> smartspace
nano telegraf.conf
# change openhab -> smartspace

docker-compose up -d

### On grafana web interface

# Param / Add data source / InfluxDB / 
# Name: Raspi_SmartSpace_InfluxDB
# URL: http://influxdb:8086
# Basic Auth: On
# User: raspi_smartspace_telegraf
# Password: raspi_smartspace_telegraf
# Database: raspi_smartspace_telegraf
# User: raspi_smartspace_telegraf
# Paswword: raspi_smartspace_telegraf
# HTTP Method: GET
# Save & Test

# + Import 10578 / Load
# Name: Host - Raspberry Pi 4B Presence Monitoring
# Change UID$#influxdb: Raspi_Presence_InfluxDB

#---------------
# Node-RED
#---------------

docker run -it \
        --name nodered \
        --net=host \
        -p 1880:1880 \
        -d \
        --restart=always \
        nodered/node-red
