import re

template="..."

dir="without_perso_ids"

fields = ["apikey", "location", "serialNumber", "geolocation", "email", "password", "serial", "secret", "clientId", "clientSecret", "username", "id", "parentId", "security_networkkey", "macAddress", "appToken", "regionCenterLocation", "internalAuthId"]

filenames = ["airquality", "amazonechocontrol", "astro", "daikin", "freebox", "gpstracker", "ipcamera", "lgwebos", "mqtt", "neato", "netatmo", "zwave"]

for filename in filenames:
    filename_in = filename + ".things"
    print(filename_in)
    f_out = open(dir + "/" + filename_in, 'w')
    with open(filename_in, 'r') as f_in:
        text = f_in.read()
        for field in fields:
            text = re.sub(field + '=\"([^\"]*)\"', field + '=\"' + template + '\"', text)
        f_out.write(text)
    f_out.close()