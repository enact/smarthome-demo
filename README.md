# Smarthome Demo

This repository contains all the necessary resources to run the final revision of the Smarthome demo. Enablers involved in this demo are: GeneSIS, ACM, BDA, "Test and Simulation", "Security and Privacy Control and Monitoring".

## Install removable hardware

- Place and power on the sound sensor platform at the right place
- Place and power on the vaccum cleaner in front of the sliding french window
- Place and power on the outdoor sensor

## Deploy ENACT enablers

Manually start the sound analysis Python script on "Buro" host (the "cloud" processing part is not included in any GeneSIS deployment model).

With GeneSIS, load and deploy the enablers with genesis/deployment-model-enablers.json

## Initialize demo

### Bootstrap (first deployement)

With a new GeneSIS instance deploy the genesis/deployment-model-step1.json

### Ready to go

- With a browser, open:
  - "Demo control" dashboard: http://192.168.0.12:2000/ui/ 
  - Switch on the Alexa and Camera devices
  - Click on the "Init" button

Start the Foscam app to connect to the camera.

Start a Shell connexion on smartspace to view the SMOOL logs:
- ssh pi@smartspace.local
  - tail -f SMOOL.txt

Ready to start demo!

### Open URL

With a new browser window, open:
- Apps:
  - SmartSpace: http://192.168.0.27:1880/#flow/3e32ec49.59e584
  - UserconfortApp: http://192.168.0.21:1880/#flow/b0f8ef2a.9ea508
  - Dashboard: http://192.168.0.21:1880/ui/#!/2
  - CommunicationCenterApp: http://192.168.0.21:1881/#flow/895245bd.0f1908
  - PrivateApp: http://192.168.0.21:1879/#flow/731371b0.b4e1c8
  - ArduinosApp: http://192.168.0.27:1881/#flow/68b1ed19.a12d44
  - PresenceApp: http://192.168.0.27:1881/#flow/68b1ed19.a12d44
  - FakeCallApp: http://192.168.0.27:1890/#flow/5827f683.c8ed7
  - BDA Dashboard: http://192.168.0.2:3000/d/vpb7SZQMz/behavioural-drift-analysis?orgId=1&refresh=10s
- Enablers:
  - GeneSIS: http://192.168.0.11:8880/
  - ACM: http://192.168.0.12:3333/
  - Test & Simlation: http://192.168.0.12:3333/
  - BDA: http://192.168.0.12:3333/

With another browser, open: 
- PhoneCallUI: http://192.168.0.27:1890/ui/#!/0

Disable the Firefox personal toolbar (to avoid displaying unseful data).

## Demo Scenario

### Show already deployed applications

The UserComfortApp is composed if multiple applications and uses different services already deployed. 

- Show the different node-red flows constituting the "UserComfortApp"
- Show the "Home Control" dashboard (alert, temperature monitoring, home control, TV control)
- Show the GeneSIS model already deployed
  - Show the ENACTProducer component and focus on the security_policy
- Shown the behavioural drift monitoring dashboard with Grafana

### First DevOps loop

Add the "CommunicationCenterApp" to the existing system.

At Dev:

1. Test and Simulation
  - With "testcampaingn-com-center" selected, click on "Launch" to start the test campaign
  - Show the "testcampaign" components by clicking on it
    - Click on "test-case-com-center-abnormal"
	- Click on "smarthome-generated-smartphone-ab-011" to show the dataset in this test
  - Show the way this "testcampaign" was created
    - Click on "Topology" in the menu bar
	- Click on "SmartHome Generator"
	- Click on "Communication Center App"
	- Click on edit for "SmartPhone Status"
	- Show the different parameters
  - Go back to "Test Campaign" to show the results 
    - Click on "Report" to access to the "testcampaign" results
  - Before leaving "Test and Simultaion"
   - Click on "Select" for "test-campaign-com-center-user-comfort"
   - Click on "Save"
   
2. Actuation Conflict Management
  - With GeneSIS
    - Load the new deployment model via "Model", "Load Deployment Model from file"
	- Select "deployment-model-step2.json" and click on "OK"
	- Click on "Deployment" and "Check Acttuation Conflicts" to test if there are actuation conflicts
  - With Actuation Conflict Management
    - Click on "Load Deployement Model from ACM Server buffer"
	- Select "Manage Conflict" and click on "Find conflicts"
	- Click on "Solve conflicts usin default ACM"
	- Replace the default ACM for "TV Mute" flow by selecting the "OR-openhab-buffered" off-the-shelf Actuation Conflict Manager
	- Select "Deploy new model" and click on "Deploy to target" via the "Deploy" menu bar
	- Enter the GeneSIS IP address: http://192.168.0.11:8880 and click on "OK"
  - Go back to GeneSIS to see the deployment (orange nodes are the only ones updated)

3. Security Policy update
  - On GeneSIS
    - Show the added jCasbin component
	- Show the updated "ENACTProducer" component with the new security checker (file reference added)
	- Show the test results updated in GeneSIS

At Ops:

4. Non regression testing with "Test and Simulation"
  - With "Test and Simulation"
    - Select report to see the testing results.
  - Show the real world with the camera
    - Show the SMOOL console log and send a toast message to the TV (should see the message on the TV and the message in log)
    - Start a Phone call that should mute the TV
    - Try to unmute with the remote control (should not work until the phone call is on)
    - Unzoom the camera to see the vacuum cleaner and start it
  - With the Grafana dashboard, show the behavioural drift on the noise parameters

### Second DevOps loop

At Dev:

5. With Behavioural Drift Analysis
  - Switch from "Configuration" to "Analysis"
  - Click on "Load dot" and select "expected-model.dot" file
  - Click on "Load expected behaviour" and select "demo-enact-expected.csv"
  - Click on "Load field behaviour" and select "demo-enact-unexpected.csv" (these two last files are extracted from influxdb)
  - Click on "Analyse"
  - Show the red new component in the FSM corresponding to a high level of noise, not expected in the defined model.

The vacuum cleaner is programmed during working hours (with nobody at home). But, due to COVID-19, we are now working at home.

6. With Actuation Conflict Management
  - Select "Environment model"
  - Click on the node before the "BotvacD3_Command" node
  - Assign it to the "Living Room Sound Control" physical process
  - Select "Conflict identification" and click on "Find conflicts" in the "Manage Conflicts" menu
  - Show the "Unmanaged conflict" fields with "1"
  - Go the the "Grid view" and select "Only ACM flows". The flow with the unresolved conflict is identified with an exclamation mark. Click on "Action" to go to this part of the application
  - Select "Conflict solving" and one the new ACM
  - Select the FSM component to specify the right behaviour with the ECA+ language
  - Copy/Paste the code
  - Click on "Verify your ECA"
  - Click on "Go!" to insert a component with this specified behaviour
  - Select "Deploy new model" and click on "Deploy to target" of the "Deploy" menu
  - Go to GeneSIS to see the end of deployment

At Ops:

  - Show the real world to see the solve conflict (if a phone call arrive, and TV is on but muted (or off), the vaccum cleaner stops (show the vacuum cleaner status)