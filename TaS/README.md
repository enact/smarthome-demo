# Test and Simulation

### Start the application

```
docker compose up
```

### Verify the addresses
- GeneSIS webhook: Dashboard -> Test Campaign -> Webhook address
- Mongodb: Dashboard -> Data Storage (192.168.0.27)
- MQTT-Broker
  + Dashboard -> Topology -> Smarthome Replayer.js -> Device List -> Setup connection to Testing Target (192.168.0.21)
  + Dashboard -> Topology -> Smarthome Replayer Remotely.js -> Device List -> Setup connection to Testing Target: (192.168.0.27)
  + NodeRED dashboard -> mqtt in/out nodes
    - mqtt-local-broker: 192.168.0.21
    - mqtt-broker-smartspace: 192.168.0.27