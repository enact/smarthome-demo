#!/bin/bash

mongod --fork --logpath /var/log/mongodb.log
#mongorestore /tmp/dump/
mongorestore --nsInclude 'smarthome_db.*' /TaS_database/backup_db
mongod --shutdown
mongod --bind_ip_all
