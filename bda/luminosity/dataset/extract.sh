#!/bin/bash

# FIELDS="Arduino2_Sensor_Luminance\|Rollershutter2_Blinds_State\|WhiteLight2_Switch_Dimmer"
# grep "$FIELDS" 2021-01-17_09-35-30_BCD_Brightness-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//' > values.ifdb
# awk '{print $3 " " $1 " " $2}' values.ifdb | sort > 2021-01-17_09-35-30_Extracted_values.ifdb
# rm values.ifdb

FIELDS="Arduino2_Sensor_Luminance\|Arduino3_Sensor_IR\|Rollershutter2_Blinds_State\|WhiteLight2_Switch_Dimmer"
grep "$FIELDS" 2021-02-03_14-11-00_BDC_Brightness-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//' > values.ifdb
awk '{print $3 " " $1 " " $2}' values.ifdb | sort > 2021-02-03_14-11-00_Extracted_values.ifdb
rm values.ifdb

FIELDS="Arduino2_Sensor_Luminance\|Arduino3_Sensor_IR\|Rollershutter2_Blinds_State\|WhiteLight2_Switch_Dimmer"
grep "$FIELDS" 2021-03-03_14-30-00_BDC_Brightness_Diff-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//' > values.ifdb
awk '{print $3 " " $1 " " $2}' values.ifdb | sort > 2021-03-03_14-30-00_Extracted_values.ifdb
rm values.ifdb