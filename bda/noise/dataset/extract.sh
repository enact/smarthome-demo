#!/bin/bash

# tr ',' ' ' | cut -d ' ' -f 1,3- |

FIELDS="TV_Power\|TV_Mute\|Smartphone_Ringing\|Smartphone_Onhook\|Mic1_ZCR\|Mic1_MFCC"

# DATE="2021-02-11_13-12-00"
# FILE="BDC_Noise_nodrift"

# grep "$FIELDS" ${DATE}_${FILE}-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//g' > values1.ifdb
# grep "$FIELDS" ${DATE}_${FILE}-personal.ifdb | sed 's/value=//g' > values2.ifdb
# cat values1.ifdb values2.ifdb | sed 's/0i/0/' | sed 's/1i/1/' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
# rm values1.ifdb values2.ifdb


# DATE="2021-02-11_14-53-00"
# FILE="BDC_Noise_drift"

# grep "$FIELDS" ${DATE}_${FILE}-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//g' > values1.ifdb
# grep "$FIELDS" ${DATE}_${FILE}-personal.ifdb | sed 's/value=//g' > values2.ifdb
# cat values1.ifdb values2.ifdb | sed 's/0i/0/' | sed 's/1i/1/' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
# rm values1.ifdb values2.ifdb

# DATE="2021-02-12_10-07-00"
# FILE="BDC_Noise"

# grep "$FIELDS" ${DATE}_${FILE}-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//g' > values1.ifdb
# grep "$FIELDS" ${DATE}_${FILE}-personal.ifdb | sed 's/value=//g' > values2.ifdb
# cat values1.ifdb values2.ifdb | sed 's/0i/0/' | sed 's/1i/1/' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
# rm values1.ifdb values2.ifdb

# DATE="2021-02-12_13-28-00"
# FILE="BDC_Noise"

# grep "$FIELDS" ${DATE}_${FILE}-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//g' > values1.ifdb
# grep "$FIELDS" ${DATE}_${FILE}-personal.ifdb | sed 's/value=//g' > values2.ifdb
# cat values1.ifdb values2.ifdb | sed 's/0i/0/' | sed 's/1i/1/' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
# rm values1.ifdb values2.ifdb

DATE="2021-02-13_07-48-00"
FILE="BDC_Noise"

grep "$FIELDS" ${DATE}_${FILE}-openhab.ifdb | tr ',' ' ' | cut -d ' ' -f 1,3- | sed 's/value=//g' > values1.ifdb
grep "$FIELDS" ${DATE}_${FILE}-personal.ifdb | sed 's/value=//g' > values2.ifdb
cat values1.ifdb values2.ifdb | sed 's/0i/0/' | sed 's/1i/1/' | sed 's/\./,/g' | awk '{print $3 " " $1 " " $2}' | sort > ${DATE}_Extracted_values.ifdb
rm values1.ifdb values2.ifdb