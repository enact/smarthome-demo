{
    "request": {
        "sequence": [
            {"custom_default": 5}
        ]
    },
    "custom_rules":[
        {"custom_addACMForSoft":{"sequence":[{"AddACM":1},{"AddACM2":20}]} },
        {"custom_addACMForAction":{"sequence":[{"AddACMToAction":1},{"AddACMToAction2":20}]} },
        {"custom_physicalConflict":{"sequence":[{"physicalConflict":1},{"physicalConflict2":20}]} },
        {"custom_default":{"sequence": [{"custom_physicalConflict": 20},{"custom_addACMForAction": 20} ]} }
    ]
}