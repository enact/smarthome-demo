# ENACT Producer

Template for the *smart home* IoT app using SMOOL middleware.

This app reads actuation commands sent from SMOOL and forwards sensor data to SMOOL

We only focus on the TV in this demo for security control

The main actuation command is ACM_GeneSIS_Demo_Common.actuator_tv_toast

## Requisites

- Java 8 or upper must be installed

## Usage

Normal execution (use the .sh file or execute the following command):

```sh
java -cp bin:lib/* ENACTConsumer.logic.ConsumerMain
```

Optionally, to start on different SMOOL server than the default, use:

```sh
java -cp bin:lib/* ENACTConsumer.logic.ConsumerMain sibName server.address port
```

## Usage (for insecure actuation detection scenario)

To send non secure actuation orders, execute the following command:

```sh
java -cp bin:lib/* -Dinsecure="true" ENACTConsumer.logic.EnactConsumerMain
```

The app will send SMOOL actuation orders but without the security concepts. The *Security Agent* listening SMOOL data will detect these orders and send event to the security monitoring tool.

