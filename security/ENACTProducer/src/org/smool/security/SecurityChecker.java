package org.smool.security;

import java.io.IOException;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.smool.kpi.model.smart.AbstractOntConcept;
import org.smool.kpi.model.smart.slots.FunctionalSlot;

import ENACTProducer.logic.ACM_GeneSIS_Demo_Common;
import ENACTProducer.model.smoolcore.IInformation;
import ENACTProducer.model.smoolcore.ISecurity;

/**
 * -----------------------------------------------------------------------------------------------------------------------------
 * Test if security constraints are met based on policies.
 * 
 * Note that the implementation of this class can be generated from external
 * tools (i.e:thingml)
 * -----------------------------------------------------------------------------------------------------------------------------
 */
public class SecurityChecker<T extends AbstractOntConcept> implements Predicate<T> {

	private Map<String, String> policies = new HashMap<>();

	public SecurityChecker() {
		// policies.put("BlindPositionActuator", "Authentication");
		policies.put("BlindPositionActuator", "Authorization");
		policies.put(ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic, "Authorization");
		// policies.put("BlindPositionActuator", "Confidentiality");
		// policies.put("BlindPositionActuator", "Integrity");
		// policies.put("BlindPositionActuator", "NonRepudiation");
	}

	/**
	 * Test if security policies are fulfilled.
	 * <p>
	 * If test is not passed, the message is discarded promptly, so the message is
	 * not arrived at the app "logic code"
	 * </p>
	 * <p>
	 * Note: This code can be replaced by custom security controls. Instead of
	 * checking policies as a HashMap, we could perform check operations on external
	 * IAM providers and so on.
	 * </p>
	 */
	public boolean test(T t) {
		try {
			if (policies.size() == 0)
				return true;
			final String name = t.getClass().getSimpleName();
			if (!policies.containsKey(name))
				return true; // there is no policy set for this element, so it is ok
			
			// get the slot with security data
			List<ISecurity> items = new ArrayList<>();
			t._listSlots().stream().filter(el -> el.isFunctionalProperty()).forEach(el -> {
				FunctionalSlot fSlot = ((FunctionalSlot) el);
				if (fSlot.getValue() == null)
					return;
				if ((fSlot.getValue() instanceof IInformation) == false)
					return;
				IInformation info = (IInformation) fSlot.getValue();
				
				ISecurity security = info.getSecurityData();
				if (security != null)
					items.add(security);
			});

			// check security elements (security objects have CLASS (Authentication
			// Authorization, etc) TYPE (KERBEROS,JWT...) and DATA(encripted payload,
			// token...
			boolean isCompliant = items.stream()
					.map(sec -> policies.get(name).equals(sec.getClass().getSimpleName().replace("Security", "")))
					.allMatch(el -> el == true);
			
			//NEW: ignore old actuation commands that sent and buffered more than 1 minute ago
			for(ISecurity sec : items){
				long timeStamp = Long.parseLong(sec.getTimestamp());
				long currentTimeStamp = System.currentTimeMillis();
				
				long diff = currentTimeStamp - timeStamp;
				
//				System.out.println("Message Timestamp = " + timeStamp); //TODO: check the timestamp to only receive the real-time ones.
//				System.out.println("Current Timestamp = " + currentTimeStamp);
				if(diff/60000 > 1) {//ignore
					return false;
				}
			};
			
			// return test
			System.out.println(">>>>>>>>>>Security checker is " + isCompliant + " for " + t._getIndividualID());
			
			//NEW: check RBAC if security key is valid
			if(isCompliant) {
				boolean isAuthorized = false;
				for(ISecurity sec: items) {
					String jwtToken = sec.getData();
					isAuthorized = getPermission(jwtToken);
					
					System.out.println(">>>>>>>>>>Security Access Control is " + isAuthorized + " for " + t._getIndividualID());
					return isAuthorized;
				}
			}
			
			return isCompliant ? true : false;

		} catch (Exception e) {
			e.printStackTrace();
			return false; // cannot verify if secure or not -> return false
		}
	}
	
	//NEW: get permission from the REST RBAC service
		private boolean getPermission(String jwtToken) {
			boolean isAuthorized = false;
            
            CloseableHttpClient httpClient = HttpClients.createDefault();

            try {

                try {

                    HttpGet request = new HttpGet("http://localhost:8011/getPermission/jwtToken/"+jwtToken);

                    // add request headers
                    request.addHeader("Content-Type", "text/plain;charset=UTF-8");

                    CloseableHttpResponse response = httpClient.execute(request);

                    try {

                        // Get HttpResponse Status
                        System.out.println(response.getProtocolVersion());              // HTTP/1.1
                        System.out.println(response.getStatusLine().getStatusCode());   // 200
                        System.out.println(response.getStatusLine().getReasonPhrase()); // OK
                        System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK



                        HttpEntity entity = response.getEntity();
                        if (entity != null) {
                            // return it as a String
                            String result = EntityUtils.toString(entity);
                            isAuthorized = (response.getStatusLine().getStatusCode() < 299) && result.equalsIgnoreCase("true");
                        }

                    } finally {
                        response.close();
                    }
                } finally {
                    httpClient.close();
                }

            }catch(java.io.IOException e){
                
            }
			
			return isAuthorized;
		}

}
