package ENACTProducer.logic;

import java.io.IOException;
import java.util.Observer;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.smool.kpi.model.exception.KPIModelException;

import ENACTProducer.api.BlindPositionActuatorSubscription;
import ENACTProducer.api.Consumer;
import ENACTProducer.api.SmoolKP;
import ENACTProducer.model.smoolcore.IContinuousInformation;
import ENACTProducer.model.smoolcore.impl.BlindPositionActuator;
import ENACTProducer.model.smoolcore.impl.Message;

/**
 * Producer of data for the Smart Building use case for Enact.
 * <p>
 * NOTE: although this app is mainly a producer of sensors data, it can receive
 * actuation orders.
 * </p>
 * <p>
 * This app is sending (via SMOOL) the temperature, gas, etc stataus in the
 * smart building. It is also receiving (via SMOOL) actuation orders, and checking if security data is valid and then it calls
 * the Smart Building SCADA to tune the values.
 * </p>
 *
 */
public class EnactProducerMain implements MqttCallback{
	
	private IMqttClient publisher; 
	

	public EnactProducerMain(String sib, String addr, int port) throws Exception {
		String name = ACM_GeneSIS_Demo_Common.kpName + System.currentTimeMillis() % 10000;
		SmoolKP.setKPName(name);
		System.out.println("KPName*** " + name + " *** Vendor:" + ACM_GeneSIS_Demo_Common.vendor);
		// ---------------------------CONNECT TO SMOOL---------------------
		// SmoolKP.connect();
		// SmoolKP.connect("sib1", "172.24.5.151", 23000);
		// SmoolKP.connect("sib1", "192.168.1.128", 23000);
		SmoolKP.connect(sib, addr, port);
		
		//Create an MQTT client here
		initMQTTclient();


//		// ---------------------------CREATE SENSOR-----------------------------
//		Producer producer = SmoolKP.getProducer();
//		
//		ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_channel = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channel_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_channelName = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channelName_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_mute = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_mute_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_power_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_volume = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_volume_id);	
		

		// ---------------------------PRODUCE DATA----------------------------------
//		String timestamp = Long.toString(System.currentTimeMillis());
//
//		Message msg = new Message();
//		msg.setBody("init Sensors from EnactProducerMain");
//		msg.setTimestamp(timestamp);
		
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channel._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channelName._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_mute._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_power._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_volume._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		

		// ---------------------------CONSUME ACTION----------------------------------
//		Consumer consumer = SmoolKP.getConsumer();
//		
//		MessageReceiveSensorSubscription subscription = new MessageReceiveSensorSubscription(createObserver());
		
//		ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_channel = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_channel_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mute = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mute_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_power_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_toast = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_toast_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_volume = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_volume_id);
		
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_channel._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_mute._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_power._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_toast._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.actuator_tv_volume._getIndividualID());
		
		// ---------------------------CONSUME ACTION----------------------------------
		Consumer consumer = SmoolKP.getConsumer();
		BlindPositionActuatorSubscription subscription = new BlindPositionActuatorSubscription(createObserver());
		consumer.subscribeToBlindPositionActuator(subscription, null);
		
		
		// -----------ATTACH WATCHDOG instead of SLEEP-------
		SmoolKP.watchdog(360000); // maximum interval that at least one message should arrive
//		Thread.sleep(1800);
	}
	
	private void initMQTTclient() throws InterruptedException {
		try{
        	// ---------------------------MQTT Client----------------------------------
    		String publisherId = UUID.randomUUID().toString();
    		//Connect to the MQTT broker that is connected to HomeIO app
    		publisher = new MqttClient("tcp://192.168.0.27:1883", publisherId);//Stephane's IoT Smart Space MQTT broker
//    		publisher = new MqttClient("tcp://localhost:1883", publisherId);
//    		publisher = new MqttClient("tcp://10.0.0.13:1883", publisherId);
            
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
			publisher.setCallback(this);
            publisher.connect(options);
			System.out.println("Connected to the broker to SMART HOME");
			
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher_topic, 0);
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_channel_topic, 0);
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_channelName_topic, 0);
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_mute_topic, 0);
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_power_topic, 0);
//			publisher.subscribe(ACM_GeneSIS_Demo_Common.sensor_tv_volume_topic, 0);
			
		}catch(MqttException e){
//        	e.printStackTrace();
			Thread.sleep(5000);
			System.out.println("WAITING for the CONNECTION to the broker to SMART HOME...");
			initMQTTclient();
            //throw new RuntimeException("Exception occurred in creating MQTT Client");
        }catch(Exception e) {
        	//Unable to connect to server (32103) - java.net.ConnectException: Connection refused
        	e.printStackTrace();
			System.exit(1);
        }
	}
	
	/**
	FROM APP to SMOOL
	*/
	 @Override
    public void connectionLost(Throwable t) {
        System.out.println("Connection to SMART HOME lost!");
        
        // code to reconnect to the broker would go here if desired
        try {
        	System.out.println("RECONNECTING to the broker to SMART HOME...");
			initMQTTclient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
		//Sending sensor data from SMART HOME to SMOOL
        	
		String message_payload = new String(mqttMessage.getPayload());
		System.out.println("-------------------------------------------------");
		System.out.println("Forwarding to SMOOL: " + s + " = " + message_payload);
		System.out.println("-------------------------------------------------");
		
		String timestamp = Long.toString(System.currentTimeMillis());
		
		Message msg = new Message();
		msg.setBody(message_payload);
		msg.setTimestamp(timestamp);
		
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_channel_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channel._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_channelName_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channelName._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_mute_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_mute._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_power_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_power._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
		if(s.equals(ACM_GeneSIS_Demo_Common.sensor_tv_volume_topic)){
			SmoolKP.getProducer().updateMessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_volume._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		}
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
//        try {
//			System.out.println("Forwarding to SMOOL completed" + new String(iMqttDeliveryToken.getMessage().getPayload()));
//		} catch (MqttException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
    }

    /**
	 * Processs messages related to any Actuator of ACM_GeneSIS_Demo
	 */
	private Observer createObserver() {
		return (o, concept) -> {
//			MessageReceiveSensor actuator = (MessageReceiveSensor) concept;
//			IMessage msg_actuator = actuator.getMessage();
			
			BlindPositionActuator actuator = (BlindPositionActuator) concept;
			IContinuousInformation info = actuator.getBlindPos();
			
			long timeStamp = Long.parseLong(info.getTimestamp());
			long currentTimeStamp = System.currentTimeMillis();
			
			long diff = currentTimeStamp - timeStamp;
			
//			System.out.println("Message Timestamp = " + timeStamp); //TODO: check the timestamp to only receive the real-time ones.
//			System.out.println("Current Timestamp = " + currentTimeStamp);
			if(diff/60000 < 1) {//ONLY commands with a time stamp less than 1 minute ago can be forwarded to actuators
					
				//ONLY If the actuator does belong to this kpName, we will process further
				if(actuator.getDeviceID().startsWith(ACM_GeneSIS_Demo_Common.kpName)) {
					
	//				System.out.println(actuator.getDeviceID() + " receiving Actuator Message order. Value: " + msg_actuator.getBody());
					
					if (info.getDataID() != null) {
						System.out.println("receiving ACTUATION order for: " + info.getDataID() + " and security "
								+ info.getSecurityData().getData());
						
						MqttMessage msg=null;
						
						String toastMsg = ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic + ": " + info.getDataID() ;
						msg = new MqttMessage(toastMsg.getBytes());
						
						if(msg!=null){
							msg.setQos(0);
							msg.setRetained(true);
							
							try {
								if(publisher.isConnected()) {
									System.out.println("-------------------------------------------------");
									System.out.println("Is Forwarding Actuation Cmd to TV: " + new String(msg.getPayload()));
									System.out.println("-------------------------------------------------");
									
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher_topic, msg);
	//								}
	//								
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_channel_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_channel_topic, msg);
	//								}
	//								
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer_topic, msg);
	//								}
	//								
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop_topic, msg);
	//								}
	//								
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_mute_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_mute_topic, msg);
	//								}
	//								
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_power_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_power_topic, msg);
	//								}
									
									//Important one
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_toast_id)) {
										publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic, msg);
	//								}
									
	//								if(actuator._getIndividualID().equals(ACM_GeneSIS_Demo_Common.actuator_tv_volume_id)) {
	//									publisher.publish(ACM_GeneSIS_Demo_Common.actuator_tv_volume_topic, msg);
	//								}
								}else {
									System.out.println("Cannot send to TV: publisher.isConnected() = " + publisher.isConnected());
								}
							} catch (MqttPersistenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (MqttException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}    
						}
					}
				}
			}
		};
	}


	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		// Logger.setDebugging(true);
		// Logger.setDebugLevel(4);
		while (true) {
			try {
				new EnactProducerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				Thread.sleep(1000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
