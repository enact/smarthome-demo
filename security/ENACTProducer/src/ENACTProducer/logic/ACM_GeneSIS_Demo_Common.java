package ENACTProducer.logic;

import ENACTProducer.model.smoolcore.impl.MessageReceiveSensor;
//import ENACTConsumer.model.smoolcore.impl.MessageSendActuator;

public class ACM_GeneSIS_Demo_Common {

	public static final String vendor = "CNRS";
	public static final String kpName = "ACM_GeneSIS_Demo_LG_TV";//"EnactProducer" + System.currentTimeMillis() % 10000;
	
	//TV sensors
	public static MessageReceiveSensor sensor_tv_appLauncher;
	public static MessageReceiveSensor sensor_tv_channel;
	public static MessageReceiveSensor sensor_tv_channelName;
	public static MessageReceiveSensor sensor_tv_mute;
	public static MessageReceiveSensor sensor_tv_power;
	public static MessageReceiveSensor sensor_tv_volume;
	
	//TV actuators
	public static MessageReceiveSensor actuator_tv_appLauncher;
	public static MessageReceiveSensor actuator_tv_channel;
	public static MessageReceiveSensor actuator_tv_mediaPlayer;
	public static MessageReceiveSensor actuator_tv_mediaStop;
	public static MessageReceiveSensor actuator_tv_mute;
	public static MessageReceiveSensor actuator_tv_power;
	public static MessageReceiveSensor actuator_tv_toast;
	public static MessageReceiveSensor actuator_tv_volume;
	
	
	//NEW: TV ids actuators
	public static final String actuator_tv_appLauncher_id = kpName + "_actuator_tv_appLauncher";
	public static final String actuator_tv_channel_id = kpName + "_actuator_tv_channel";
	public static final String actuator_tv_mediaPlayer_id = kpName + "_actuator_tv_mediaPlayer";
	public static final String actuator_tv_mediaStop_id = kpName + "_actuator_tv_mediaStop";
	public static final String actuator_tv_mute_id = kpName + "_actuator_tv_mute";
	public static final String actuator_tv_power_id = kpName + "_actuator_tv_power";
	public static final String actuator_tv_toast_id = kpName + "_actuator_tv_toast";
	public static final String actuator_tv_volume_id = kpName + "_actuator_tv_volume";
	
	//NEW: TV ids sensors
	public static final String sensor_tv_appLauncher_id = kpName + "_sensor_tv_appLauncher";
	public static final String sensor_tv_channel_id = kpName + "_sensor_tv_channel";
	public static final String sensor_tv_channelName_id = kpName + "_sensor_tv_channelName";
	public static final String sensor_tv_mute_id = kpName + "_sensor_tv_mute";
	public static final String sensor_tv_power_id = kpName + "_sensor_tv_power";
	public static final String sensor_tv_volume_id = kpName + "_sensor_tv_volume";
	
	//NEW: TV TOPICS for actuators
	public static final String actuator_tv_appLauncher_topic = "enact/actuators/tv/appLauncher";
	public static final String actuator_tv_channel_topic = "enact/actuators/tv/channel";
	public static final String actuator_tv_mediaPlayer_topic = "enact/actuators/tv/mediaPlayer";
	public static final String actuator_tv_mediaStop_topic = "enact/actuators/tv/mediaStop";
	public static final String actuator_tv_mute_topic = "enact/actuators/tv/mute"; //"ON" "OFF"
	public static final String actuator_tv_power_topic = "enact/actuators/tv/power"; //"ON" "OFF"
	public static final String actuator_tv_toast_topic = "enact/actuators/tv/toast";
	public static final String actuator_tv_volume_topic = "enact/actuators/tv/volume";
	
	//NEW: TV TOPICS for sensors
	public static final String sensor_tv_appLauncher_topic = "enact/sensors/tv/appLauncher";
	public static final String sensor_tv_channel_topic = "enact/sensors/tv/channel";
	public static final String sensor_tv_channelName_topic = "enact/sensors/tv/channelName";
	public static final String sensor_tv_mute_topic = "enact/sensors/tv/mute"; //"ON" "OFF"
	public static final String sensor_tv_power_topic = "enact/sensors/tv/power"; //"ON" "OFF"
	public static final String sensor_tv_volume_topic = "enact/sensors/tv/volume";
}
