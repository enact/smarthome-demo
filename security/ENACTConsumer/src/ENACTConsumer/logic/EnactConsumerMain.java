package ENACTConsumer.logic;

import java.io.IOException;
import java.util.Observer;
import java.util.UUID;

import org.smool.kpi.model.exception.KPIModelException;

import ENACTConsumer.api.Consumer;
import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.api.TemperatureSensorSubscription;
import ENACTConsumer.model.smoolcore.IMessage;
import ENACTConsumer.model.smoolcore.impl.BlindPositionActuator;
import ENACTConsumer.model.smoolcore.impl.Message;
import ENACTConsumer.model.smoolcore.impl.MessageReceiveSensor;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;
import ENACTConsumer.model.smoolcore.impl.TemperatureSensor;
import ENACTConsumer.api.MessageReceiveSensorSubscription;
import ENACTConsumer.api.Producer;

import org.eclipse.paho.client.mqttv3.*;

/**
 * Subscribe to data generated from KUBIK smart building.
 * <p>
 * This class should be the skeleton for the IoT application in the SB use case
 * </p>
 * <p>
 * The app is retrieving temperature and other data. When temp is higher or
 * lower than comfort, an actuation order is sent back to the Smart Building to
 * turn the temperature back to normal.
 * </p>
 *
 */
public class EnactConsumerMain implements MqttCallback {	
//	public static TVActuation actuation;
	public static CustomTVActuation actuation;

	//for brokering to NodeRED apps
	public IMqttClient publisher; 
    

	public EnactConsumerMain(String sib, String addr, int port) throws Exception {

		SmoolKP.setKPName(ACM_GeneSIS_Demo_Common.kpName + System.currentTimeMillis() % 10000);
		System.out.println("*** " + ACM_GeneSIS_Demo_Common.kpName + " ***");
		
		// ------------PREPARE ACTUATION OBJECT--------------------
		actuation = new CustomTVActuation(ACM_GeneSIS_Demo_Common.kpName);

		// ---------------------------CONNECT TO SMOOL---------------------

		SmoolKP.connect(sib, addr, port);
		
		//Create an MQTT client here
		initMQTTclient();

		// ---------------------------SENSOR DATA----------------------
//		Consumer consumer = SmoolKP.getConsumer();
//
//		ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_channel = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channel_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_channelName = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_channelName_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_mute = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_mute_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_power_id);
//		ACM_GeneSIS_Demo_Common.sensor_tv_volume = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.sensor_tv_volume_id);
//
//
//		//Subscribe to receive sensor data from SMOOL
//		MessageReceiveSensorSubscription subscription = new MessageReceiveSensorSubscription(createObserver());
//
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_channel._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_channelName._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_mute._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_power._getIndividualID());
//		consumer.subscribeToMessageReceiveSensor(subscription, ACM_GeneSIS_Demo_Common.sensor_tv_volume._getIndividualID());

		//current time
//		String timestamp = Long.toString(System.currentTimeMillis());
//
//		Message msg = new Message();
//		msg.setBody("Test Actuation Message");
//		msg.setTimestamp(timestamp);
//
//		// ---------------------------ACTUATION DATA----------------------
//		Producer producer = SmoolKP.getProducer();
//		
//		ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_channel = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_channel_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_mute = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mute_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_power = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_power_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_toast = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_toast_id);
//		ACM_GeneSIS_Demo_Common.actuator_tv_volume = new MessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_volume_id);
		
		
//		//Initialize produced data
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_channel._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_mute._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_power._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_toast._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
//		producer.createMessageReceiveSensor(ACM_GeneSIS_Demo_Common.actuator_tv_volume._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, null, null, msg, null);
		
//		sec = new SecurityAuthorization(kpName + "_security"+Integer.toString(counter++));
//		sec.setType("JWT + CASBIN payload");
//		
//		// if started as insecure, do not send credentials (>java -Dinsecure app.jar)
//		if (System.getProperty("insecure") != null) {
//			// IMPORTANT, send data = "" instead of null since the global list of props must
//			// be sent and if old BlindSobscriptions where created with that triple and then
//			// with new insecure requests the data triple is not sent, the subscriptors
//			// crash and cannot reconnect until smool server is restarted.
//			sec.setData("");
//		} else {
//			String token = HTTPS.post("https://localhost:8443/jwt", "{\"id\":\""+kpName+"\"}");
//			sec.setData(token);
//		}
		
		// ------------SUBSCRIBE TO DATA---------------------------
//		Consumer consumer = SmoolKP.getConsumer();
//		consumer.subscribeToTemperatureSensor(new TemperatureSensorSubscription(createTemperatureObserver()), null);
		
		// -----------ATTACH WATCHDOG instead of SLEEP-------
		SmoolKP.watchdog(360000); // maximum interval that at least one message should arrive
//		Thread.sleep(1800);
	}
	
	/**
	 * Create an MQTT client here just for: 
	 * Whenever there is a message from SMOOL, send it via MQTT to apps
	 * Whenever there is an actuation order from apps via MQTT, send it to SMOOL. 
	 * @throws InterruptedException 
	 */
	private void initMQTTclient() throws InterruptedException {
		try{
        	// ---------------------------MQTT Client----------------------------------
			String publisherId = UUID.randomUUID().toString();
//    		publisher = new MqttClient("tcp://localhost:1883", publisherId);//Use the local MQTT broker 
    		publisher = new MqttClient("tcp://192.168.0.21:1883", publisherId);//Use the MQTT broker at Stephane's AppServer, not tcp://localhost:1883
    		
            
            MqttConnectOptions options = new MqttConnectOptions();
            options.setAutomaticReconnect(true);
            options.setCleanSession(true);
            options.setConnectionTimeout(10);
			publisher.setCallback(this);
            publisher.connect(options);
			System.out.println("Connected to the Apps broker");

			// All subscriptions
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_appLauncher_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_channel_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_mediaPlayer_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_mediaStop_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_mute_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_power_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic, 0);
			publisher.subscribe(ACM_GeneSIS_Demo_Common.actuator_tv_volume_topic, 0);
            
		}catch(MqttException e){
			Thread.sleep(5000);
			System.out.println("WAITING for the CONNECTION to the broker to the Apps...");
			initMQTTclient();
            //throw new RuntimeException("Exception occurred in creating MQTT Client");
        }catch(Exception e) {
        	//Unable to connect to server (32103) - java.net.ConnectException: Connection refused
        	e.printStackTrace();
			System.exit(1);
        }
	}
	
	/**
		MQTT connection FROM APP to SMOOL Consumer
	*/
	 public void connectionLost(Throwable t) {
        System.out.println("Connection lost!");
        // code to reconnect to the broker would go here if desired
        try {
        	System.out.println("RECONNECTING to the MQTT broker to Apps");
			initMQTTclient();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        
        if(topic.equals(ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic)){
			String message_payload = new String(mqttMessage.getPayload());
	    	System.out.println("-------------------------------------------------");
	        System.out.println("| Topic:" + topic);
	        System.out.println("| Message: " + message_payload);
	        System.out.println("-------------------------------------------------");
	        
			//Update the actuator
	        EnactConsumerMain.actuation.run(message_payload);
		}
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        //System.out.println("Pub complete" + new String(token.getMessage().getPayload()));

    }
    
    /**
	 * Processs messages related to ACM_GeneSIS_Demo topics sent from SMOOL
	 */
	private Observer createObserver() {
		return (o, concept) -> {
			MessageReceiveSensor sensor = (MessageReceiveSensor) concept;
			IMessage msg_sensor = sensor.getMessage();
			
			//ONLY If the sensor data does belong to ACM_GeneSIS_Demo_Common.kpName, we will process further
			if(sensor.getDeviceID().startsWith(ACM_GeneSIS_Demo_Common.kpName)) {
				
				System.out.println(sensor.getDeviceID() + " receiving Actuator Message order. Value: " + msg_sensor.getBody());
				
				if(msg_sensor.getBody() != null){
					MqttMessage msg=null;
					
					msg = new MqttMessage(msg_sensor.getBody().getBytes());
					
					if(msg!=null){
						msg.setQos(0);
						msg.setRetained(true);
						
						try {
							if(publisher.isConnected()) {
								//whenever there is sensor data update sent from SMOOL server, publish it to the Apps via MQTT broker 
								System.out.println("-------------------------------------------------");
								System.out.println("Is Forwarding Sensor Data to Apps: " + new String(msg.getPayload()));
								System.out.println("-------------------------------------------------");
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_appLauncher_topic, msg);
								}
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_channel)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_channel_topic, msg);
								}
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_channelName)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_channelName_topic, msg);
								}
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_mute)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_mute_topic, msg);
								}
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_power)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_power_topic, msg);
								}
								
								if(sensor._getIndividualID().equals(ACM_GeneSIS_Demo_Common.sensor_tv_volume)) {
									publisher.publish(ACM_GeneSIS_Demo_Common.sensor_tv_volume_topic, msg);
								}
								
							}else {
								System.out.println("Cannot send: publisher.isConnected() = " + publisher.isConnected());
							}
						} catch (MqttPersistenceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (MqttException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}    
					}
				}
			}
		};
	}
	
//	private Observer createTemperatureObserver() {
//		return (o, concept) -> {
//			TemperatureSensor sensor = (TemperatureSensor) concept;
//			double temp = sensor.getTemperature().getValue();
//			System.out.println("temp  from " + sensor._getIndividualID() + ": " + temp);
//			if (temp > 24) {
//				// launch an actuation order to modify the blinds position
//				// new Thread(() -> ConsumerMain.actuation.run(temp)).start(); // ConsumerMain.actuation.run(temp);
////				ConsumerMain.actuation.run(temp);
//			}
//		};
//	}


	public static void main(String[] args) throws Exception {
		String sib = args.length > 0 ? args[0] : "sib1";
		String addr = args.length > 1 ? args[1] : "15.236.132.74";
		int port = args.length > 2 ? Integer.valueOf(args[2]) : 23000;
		while (true) {
			try {
				new EnactConsumerMain(sib, addr, port);
			} catch (KPIModelException | IOException e) {
				e.printStackTrace();
				Thread.sleep(10000);
				System.out.println("RECONNECTING");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
