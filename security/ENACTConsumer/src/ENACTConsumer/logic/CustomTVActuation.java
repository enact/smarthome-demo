package ENACTConsumer.logic;

import ENACTConsumer.api.SmoolKP;
import ENACTConsumer.model.smoolcore.impl.ContinuousInformation;
import ENACTConsumer.model.smoolcore.impl.SecurityAuthorization;

/**
 * Example of sending back to SMOOL am actuation message with security data.
 * 
 * <p>
 * Once a temperature value is reached, the temperature Observer calls this
 * class. Then, a new Actuation message on blinds is sent. To prevent other
 * clients to send actuation orders, this message is sent with security content.
 * </P>
 */
class CustomTVActuation {
	
	private static final String SECURITY_ENDPOINT = "https://15.236.132.74:8443/jwt";
	private boolean isFirstActuation = true;
	private String kpName;
	private String name;
	private double val = 0;
	private static int counter=0;

	SecurityAuthorization sec;
	private ContinuousInformation toastCmd;
	
	public CustomTVActuation(String kpName) {
		this.kpName = kpName;
		this.name = kpName + ACM_GeneSIS_Demo_Common.actuator_tv_toast_id + System.currentTimeMillis() % 10000;
		toastCmd = new ContinuousInformation(ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic);	
		sec = new SecurityAuthorization(name + "_security");
	}
	
	/**
	 * (Phu) TODO: the sub, obj, act must be variables. 
	 * Token format example
	 * 
	 * {
  "sub": "SmartEnergyApp",
  "obj": "BlindPositionActuator_Kitchen",
  "act": "blindUp",
  "iat": 1612275250,
  "exp": 1612275850
}

	 * 
	 * @param temp
	 */
	
	
	public synchronized void run(String msg) {
		try {
			System.out.println("Sending ACTUATION order for msg = " + msg);
			
//			String token = HTTPS.post(SECURITY_ENDPOINT, "{\"sub\":\""+kpName+"\",\"obj\": \""+ACM_GeneSIS_Demo_Common.actuator_tv_toast_id+"\", \"act\": \""+ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic+"\"}");
//			sec = new SecurityAuthorization(name + "_security"+Integer.toString(counter++));
//			sec.setType("JWT + CASBIN payload").setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			// create the security metadata
//			String token = HTTPS.post(SECURITY_ENDPOINT, "{\"sub\":\""+kpName+"\",\"obj\": \"BlindPositionActuator_Kitchen\", \"act\": \"blindDown\"}");
			String token = HTTPS.post(SECURITY_ENDPOINT, "{\"sub\":\""+kpName+"\",\"obj\": \""+ACM_GeneSIS_Demo_Common.actuator_tv_toast_id+"\", \"act\": \""+ACM_GeneSIS_Demo_Common.actuator_tv_toast_topic+"\"}");
			sec=new SecurityAuthorization(name + "_security"+Integer.toString(counter++));
			sec.setType("JWT + CASBIN payload").setData(token).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			// create the blindPosition information
			toastCmd.setDataID(name).setValue(val++).setSecurityData(sec).setTimestamp(Long.toString(System.currentTimeMillis()));
			
			
			// send to SMOOL
			if (isFirstActuation) {
				SmoolKP.getProducer().createBlindPositionActuator(name, ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, toastCmd, null);
//				SmoolKP.getProducer().createBlindPositionActuator(ACM_GeneSIS_Demo_Common.actuator_tv_toast._getIndividualID(), ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, blindPos, null);
				toastCmd.setTimestamp(null);
				isFirstActuation=false;
			}else {
				SmoolKP.getProducer().updateBlindPositionActuator(name, ACM_GeneSIS_Demo_Common.kpName, ACM_GeneSIS_Demo_Common.vendor, null, toastCmd, null);
			}
		} catch (Exception e) {
			System.err.println("Error: the actuation order cannot be sent. " + e.getMessage());
		}
	}
	
	
}
