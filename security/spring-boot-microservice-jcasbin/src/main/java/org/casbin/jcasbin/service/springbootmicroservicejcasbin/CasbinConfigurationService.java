package org.casbin.jcasbin.service.springbootmicroservicejcasbin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

//@ConfigurationProperties
//https://www.techiedelight.com/read-values-from-application-properties-file-in-spring-boot/
//
@Service
public class CasbinConfigurationService implements ICasbinConfigurationService{
	@Value("${casbin.authorization.model}")
    private String path2Model;
	
	@Value("${casbin.authorization.policy}")
    private String path2Policy;
	
	public String getCasbinAuthorizationModel() {
		return path2Model;
	}

	public void setCasbinAuthorizationModel(String path2Model) {
		this.path2Model = path2Model;
	}
	
	public String getCasbinAuthorizationPolicy() {
		return path2Policy;
	}

	public void setCasbinAuthorizationPolicy(String path2Policy) {
		this.path2Policy = path2Policy;
	}
}
