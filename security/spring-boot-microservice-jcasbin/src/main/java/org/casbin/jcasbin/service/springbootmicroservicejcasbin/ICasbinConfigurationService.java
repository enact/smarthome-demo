package org.casbin.jcasbin.service.springbootmicroservicejcasbin;

public interface ICasbinConfigurationService {

	String getCasbinAuthorizationModel();
	
	String getCasbinAuthorizationPolicy();
	
}
