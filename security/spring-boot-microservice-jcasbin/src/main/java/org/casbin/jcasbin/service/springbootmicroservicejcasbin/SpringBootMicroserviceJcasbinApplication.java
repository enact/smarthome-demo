package org.casbin.jcasbin.service.springbootmicroservicejcasbin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMicroserviceJcasbinApplication implements ApplicationRunner{
	
	//@Autowired Environment env;
	//private String path2Model = env.getProperty("casbin.authorization.model");
	//private String path2Policy = env.getProperty("casbin.authorization.policy");
	
	@Value("${person.name}")
    private String name;
	
	@Value("${casbin.authorization.model}")
    private String path2Model;
	
	@Value("${casbin.authorization.policy}")
    private String path2Policy;

	public static void main(String[] args) {
		//for(String arg:args) {
        //    System.out.println(arg);
        //}
		SpringApplication.run(SpringBootMicroserviceJcasbinApplication.class, args);
	}
	
	@Override
    public void run(ApplicationArguments args ) throws Exception
    {
        System.out.println("${person.name} = " +name);
        
        System.out.println("${casbin.authorization.model} = " +path2Model);
        
        System.out.println("${casbin.authorization.policy} = " +path2Policy);
    }

}
