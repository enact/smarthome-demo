package org.casbin.jcasbin.service.springbootmicroservicejcasbin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.casbin.jcasbin.main.Enforcer;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class RbacPermissionController {
	
	@Autowired
	private ICasbinConfigurationService casbinConfig;
	
	//cannot be instantiated in the constructor of this class
	private Enforcer enforcer;
	
	@GetMapping("/getPermission/sub/{sub}/obj/{obj}/act/{act}")
	  public boolean getPermission
	    (@PathVariable String sub, @PathVariable String obj, @PathVariable String act){
		
		System.out.println("${casbin.authorization.model} = " + casbinConfig.getCasbinAuthorizationModel());
        System.out.println("${casbin.authorization.policy} = " + casbinConfig.getCasbinAuthorizationPolicy());
		
        //only here that we can get the environment variables for the casbin config. Thus init the enforcer here. 
        enforcer = new Enforcer(casbinConfig.getCasbinAuthorizationModel(), casbinConfig.getCasbinAuthorizationPolicy());
	    
		if (enforcer.enforce(sub, obj, act) == true) {
		    // e.g., permit SmartEnergyApp to change the Roller_Shades_1
			return true;
		} else {
		    // deny the request, show an error
			return false;
		}
	  }
	
	@GetMapping("/getPermission/jwtToken/{jwtToken}")
	  public boolean getPermission
	    (@PathVariable String jwtToken){
	    
		boolean isAuthorized = false;
		
		java.util.Base64.Decoder decoder = java.util.Base64.getUrlDecoder();
        String[] parts = jwtToken.split("\\."); // split out the "parts" (header, payload and signature)

        //String headerJson = new String(decoder.decode(parts[0]));
        String payloadJson = new String(decoder.decode(parts[1]));
        //String signatureJson = new String(decoder.decode(parts[2]));
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JwtPayload payloadObj;
		try {
			payloadObj = mapper.readValue(payloadJson, JwtPayload.class);
			/**
			 * using jCasbin for RBAC enforcement
			 * https://github.com/casbin/jcasbin
			 */
			
			String sub =  "\"" + payloadObj.getSub() + "\""; // the user/app that wants to access a resource.
			
			String obj = "\"" + payloadObj.getObj() + "\""; // the object that the access being requested
			
			String act = "\"" + payloadObj.getAct() + "\""; // the requested action of the sub on the obj

			//only here that we can get the environment variables for the casbin config. Thus init the enforcer here. 
			enforcer = new Enforcer(casbinConfig.getCasbinAuthorizationModel(), casbinConfig.getCasbinAuthorizationPolicy());
			
			if (enforcer.enforce(sub, obj, act) == true) {
			    // e.g., permit SmartEnergyApp to change the Roller_Shades_1
				isAuthorized = true;
			} 

			return isAuthorized;
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isAuthorized;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return isAuthorized;
		}      
		
	  }
}
