package org.casbin.jcasbin.service.springbootmicroservicejcasbin;

/**
 * 
 * @author phun
 * This is a POJO class for the JWT token from Smool Server
 * 
 * eyJhbGciOiJub25lIiwidHlwIjoiSldUIn0.eyJzdWIiOiJibGluZExpdmluZ0Rvd24iLCJvYmoiOiJCbGluZFBvc2l0aW9uQWN0dWF0b3IiLCJhY3QiOiJ3cml0ZSIsImlhdCI6MTYxMTYwMjY2MCwiZXhwIjoxNjExNjAzMjYwfQ.
 * 
 * To see, can manually decrypt data by https://jwt.io/ OR http://calebb.net/
{
 alg: "none",
 typ: "JWT"
}.
{
  "sub" : "ACM_GeneSIS_Demo_LG_TV",
  "obj" : "tv_toast",
  "act" : "enact/actuators/tv/toast",
  "iat" : 1612433411,
  "exp" : 1612433471
}.
[signature]
 *
 */

public class JwtPayload {

	private String sub;

    private String obj;
    
    private String act;

    private String iat;
    
    private String exp;

    public String getSub ()
    {
        return sub;
    }

    public void setSub (String sub)
    {
        this.sub = sub;
    }
    
    public String getIat ()
    {
        return iat;
    }

    public void setIat (String iat)
    {
        this.iat = iat;
    }

    @Override
    public String toString()
    {
        return "JWTProperties [sub = "+sub+", obj = "+obj+", act = "+act+", iat = "+iat+ ", exp = "+exp+"]";
    }

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getObj() {
		return obj;
	}

	public void setObj(String obj) {
		this.obj = obj;
	}

	public String getAct() {
		return act;
	}

	public void setAct(String act) {
		this.act = act;
	}
}
